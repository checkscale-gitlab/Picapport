ARG IMAGE=latest


# first image to download qemu and make it executable
FROM alpine AS qemu

ARG QEMU=x86_64
ARG QEMU_VERSION=v6.1.0-8
ARG VERSION=10-3-01

ADD https://github.com/multiarch/qemu-user-static/releases/download/${QEMU_VERSION}/qemu-${QEMU}-static /usr/bin/qemu-${QEMU}-static
ADD https://www.picapport.de/download/${VERSION}/picapport-headless.jar /picapport-headless.jar
RUN chmod +x /usr/bin/qemu-${QEMU}-static


# second image to deliver the picapport container
FROM alpine:${IMAGE}

MAINTAINER Briezh Khenloo


ARG QEMU
ARG VERSION

ARG ARCH=amd64

ENV PICAPPORT_LANG=de \
    PICAPPORT_PORT=80 \
    DTRACE=WARNING \
    XMS=256m \
    XMX=1024m

RUN mkdir -p /opt/picapport/.picapport && \
    printf "%s\n%s\n%s\n" "server.port=$PICAPPORT_PORT" "robot.root.0.path=/srv/photos" "foto.jpg.usecache=2" > /opt/picapport/.picapport/picapport.properties

COPY --from=qemu /usr/bin/qemu-${QEMU}-static /usr/bin/qemu-${QEMU}-static

ARG OPENJDK=openjdk11-jre
RUN apk add --update --no-cache tini $OPENJDK

COPY --from=qemu /picapport-headless.jar /opt/picapport/picapport-headless.jar

WORKDIR /opt/picapport
EXPOSE ${PICAPPORT_PORT}
#ENV TZ=CET \
#    LANG=en_US.UTF-8 \
#   LANGUAGE=en_US:en

ENTRYPOINT tini -- java -Xms$XMS -Xmx$XMX -DTRACE=$DTRACE \
    -Duser.home=/opt/picapport \
    -Duser.language=$PICAPPORT_LANG \
    -jar picapport-headless.jar

LABEL \
    de.bksolutions.picapport.version=$VERSION \
    de.bksolutions.picapport.name="Picapport" \
    de.bksolutions.picapport.vendor="BK-Solutions" \
    de.bksolutions.picapport.maintainer="Briezh Khneloo" \
    de.bksolutions.picapport.docker.cmd="docker run -d -p 8080:80 bksolutions/picapport" \
    de.bksolutions.picapport.run="docker run -rm --name picapport -p 8080:80 -v <$PWD>/photo:srv/photo -dt docker.io/bksolutions/picapport:latest" \
    de.bksolutions.picapport.architecture=$ARCH
