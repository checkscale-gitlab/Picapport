#! /usr/bin/env bash

REPO_OPEN=$1    # ARG1: Docker repository URI without authentification
REPO_LOCK=$2    # ARG2: Docker repository URI with authentification; ARG3=Username, ARG4=Passphrase


##
# Call function to tag image
#
taggen () { 
    [ ! -z $REPO_OPEN ] && docker image tag picapport:$1 ${REPO_OPEN}:$2
    [ ! -z $REPO_LOCK ] && docker image tag picapport:$1 ${REPO_LOCK}:$2
}

##
# Call function to push images to repository
#
push () {
    [ ! -z $REPO_OPEN ] && docker push ${REPO_OPEN}
    [ ! -z $REPO_LOCK ] && docker login -u $3 -p $4 && docker push ${REPO_LOCK}
}


##
# Get latest QEMU version number
#

QEMU_VERSION="v6.1.0-8"
if [[ $(which curl) ]]; then
    QEMU_VERSION=$(curl --silent "https://api.github.com/repos/multiarch/qemu-user-static/releases/latest" | grep '"tag_name":' | cut -d: -f2 | cut -d\" -f2)
elif [[ $(which wget) ]]; then
    QEMU_VERSION=$(wget -q -O -  "https://api.github.com/repos/multiarch/qemu-user-static/releases/latest" | grep '"tag_name":' | cut -d: -f2 | cut -d\" -f2)
fi


##
# Create image
#

latest=""
vers=""
apps="$(curl -s https://www.picapport.de/de/photo-server-download.php | sed -rn "s/(.*dlfile=([0-9,-]+)%2fpicapport-headless.*)/\2/p")"

# Run over JRE versions
for jdk  in openjdk11-jre \
            openjdk17-jre \
            openjdk8-jre 
do
    echo "Use JDK: $jdk"

    # Run over all available app versions 
    for ver in $apps; do
        echo "App version:  $ver..."

        tag=${ver//-/.}.${jdk}
        echo "Tag: $tag"

        docker build -t picapport:"$tag" \
            --build-arg VERSION="$ver" \
            --build-arg QEMU_VERSION="$QEMU_VERSION" \
            --build-arg OPENJDK="$jdk" \
            .

        ##
        # Tag image with app version + jdk
        taggen $tag $tag

        # Tag image with app version
        if [ -z $vers ]; then
            echo "Tag application version: $ver"
            taggen $tag ${ver//-/.}
        fi

        # Tag image as latest
        if [ -z $latest ]; then
            taggen $tag latest 
            latest=${ver//-/.}
        fi
    done
    if [ -z $vers ]; then
        vers=$jdk
    fi
done

#Show all available images
docker image ls

#Tag latest and push all images to repository
if [ ! -z $latest ]; then  
    echo "Latest container version: $latest"
    echo "Tagged versions use: $jdk"

#    docker_push
fi

##Remove all images
docker image prune -a -f
docker image ls
